package id.ac.ub.papb.recyclerview205150400111070;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    RecyclerView rv1;
    Button bt1;
    TextView etNim, etName;
    MahasiswaAdapter mhsadpt;
    ArrayList<Mahasiswa> mhs = new ArrayList<>();
    public static String TAG = "RV1";
    private MahasiswaAdapter.recycle listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setOnClickListener();
        rv1 = findViewById(R.id.rv1);
        rv1.setHasFixedSize(true);

        etNim = findViewById(R.id.etNim);
        etName = findViewById(R.id.etName);
        bt1 = findViewById(R.id.bt1);

        bt1.setOnClickListener(l ->{
            mhs.add(new Mahasiswa(etNim.getText().toString(), etName.getText().toString()));
            mhsadpt = new MahasiswaAdapter(this,mhs);
            RecyclerView.LayoutManager layman = new LinearLayoutManager(MainActivity.this);
            rv1.setAdapter(mhsadpt);
        });
        mhs.add(new Mahasiswa("1001", "Android"));
        mhs.add(new Mahasiswa("1002", "iPhone"));
        mhs.add(new Mahasiswa("1003", "Mac OS X"));
        mhs.add(new Mahasiswa("1004", "Windows"));
        mhs.add(new Mahasiswa("1005", "Linux"));
        mhs.add(new Mahasiswa("1006", "Symbian"));
        mhs.add(new Mahasiswa("1007", "Blackberry"));
        mhs.add(new Mahasiswa("1008", "Sun OS"));
        mhs.add(new Mahasiswa("1009", "Solaris"));
        mhsadpt = new MahasiswaAdapter(this, mhs);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
        rv1.setLayoutManager(layoutManager);

        mhsadpt.setOnItemClickistener((position, l) -> {
            mhsadpt = new MahasiswaAdapter(this,mhs);
            rv1.setAdapter(mhsadpt);
        });
        rv1.setAdapter(mhsadpt);
    }

    private void setOnClickListener() {
        listener = new MahasiswaAdapter.recycle() {
            @Override
            public void onClick(View v, int position) {
                Intent intent = new Intent(getApplicationContext(),Activity2.class);
                intent.putExtra("nim", mhs.get(position).getNim());
                intent.putExtra("user", mhs.get(position).getNama());
                startActivity(intent);
            }
        };
    }
//            ArrayList<Mahasiswa> data = getData();
//            MahasiswaAdapter adapter = new MahasiswaAdapter(this, data);
//            rv1.setAdapter(adapter);
//            rv1.setLayoutManager(new LinearLayoutManager(this));
//            public ArrayList getData() {
//            ArrayList<Mahasiswa> data = new ArrayList<>();
//            List<String> nim = Arrays.asList(getResources().getStringArray(R.array.nim));
//            List<String> nama = Arrays.asList(getResources().getStringArray(R.array.nama));
//            for (int i = 0; i < nim.size(); i++) {
//            Mahasiswa mhs = new Mahasiswa();
//            mhs.nim = nim.get(i);
//            mhs.nama = nama.get(i);
//            Log.d(TAG,"getData "+mhs.nim);
//            data.add(mhs);
//            }
//            return data;
//            }
}