package id.ac.ub.papb.recyclerview205150400111070;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class Activity2 extends AppCompatActivity {

    TextView etNim2,etNama2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);

        etNim2 = findViewById(R.id.etNim2);
        etNama2 = findViewById(R.id.etNama2);

        Bundle extras = getIntent().getExtras();
        String nim = extras.getString("nim");
        String user = extras.getString("user");
        etNim2.setText(nim);
        etNim2.setText(user);
    }
}